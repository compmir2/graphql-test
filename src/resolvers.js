import { users } from "./db";

const resolvers = {
  Query: {
    hello: () => "Hello World!",
    trading: () => {
        fetch('https://k-m.ru')
        .then(response => response.json())
        .then(result => { return   "trade result!"})
        .catch(e => console.log(e));
        
      return   "trade!"
    },

    user: (parent, { id }, context, info) => {
        return users.find(user => user.id === id);
      },
      users: (parent, args, context, info) => {
        return users;
      }


  }
};

export default resolvers;